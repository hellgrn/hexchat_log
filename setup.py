#!/usr/bin/env python3

from setuptools import setup

setup(
    name='hexchat_log',
    version='1.0.0b1',
    description='Creates HexChat-windows for script-output-messages.',

    author='hellgrn',
    author_email='dev@hellgrn.de',
    license='MIT',
    url='https://bitbucket.org/hellgrn/hexchat_log',

    py_modules=["hexchat_log"],
    keywords='hexchat logging',

    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
        'Topic :: Communications :: Chat :: Internet Relay Chat',
    ],
)
