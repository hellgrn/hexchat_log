import hexchat as irc


class ScriptLog:
    def __init__(self, log_name, sub_log=False, log_window='Script-Logs'):
        self.log_name = log_name
        self.log_window = log_window
        self.sub_log = sub_log
        self.context = None

    def __create_log(self):
        context = irc.find_context(server=self.log_window)
        if context is None:
            irc.command('newserver -noconnect {}'.format(self.log_window))
            context = irc.find_context(server=self.log_window)
        return context

    def __create_sublog(self):
        context = irc.find_context(server=self.log_window,
                                   channel=self.log_name)
        if context is None:
            context = self.__create_log()
            context.command('query {}'.format(self.log_name))
            context = irc.find_context(server=self.log_window,
                                       channel=self.log_name)
        return context

    def write(self, log_message, log_title=None):
        # create windows if not exists and return context
        if self.sub_log:
            self.context = self.__create_sublog()
        else:
            self.context = self.__create_log()

        # this is a fallback, if still - somehow - no context is available
        if self.context is None:
            irc.emit_print('Generic Message',
                           self.log_name,
                           log_message)
        else:
            log_name = self.log_name if log_title is None else log_title
            self.context.emit_print('Generic Message',
                                    log_name,
                                    log_message)
