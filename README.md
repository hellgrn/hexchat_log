# HexChat Log-Windows

better description coming soon \o/

## Usage:

```
#!python

from hexchat_log import ScriptLog

log = ScriptLog('Highlights', sub_log=True)
log.write('this is the highlight message', 'Nickname')

```

Without `sub_log=True` all messages are written in the same window.
The second parameter of ScriptLog.write() ist optional.